#!/bin/bash
xinput set-float-prop "AlpsPS/2 ALPS DualPoint Stick" "Device Accel Adaptive Deceleration" 80
xinput set-float-prop "AlpsPS/2 ALPS DualPoint Stick" "Device Accel Constant Deceleration" 2
xinput set-float-prop "AlpsPS/2 ALPS DualPoint Stick" "Device Accel Velocity Scaling" 0.4
xinput set-float-prop "AlpsPS/2 ALPS DualPoint Stick" "Evdev Wheel Emulation Inertia" 35
# xinput set-prop "15" "Device Enabled" 0 # to disable trackpoint
# xinput list # to see all input devices
# xinput list-props "device name" # to see device parameters
