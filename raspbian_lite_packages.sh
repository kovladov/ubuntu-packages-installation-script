#!/bin/bash

sudo apt update
sudo apt upgrade

sudo apt install -y python3-pip # python3 package installer
pip3 install pandas # python library to work with data
pip3 install request # python library to work with api
pip3 install python-binance # python library to  work with binance api
pip3 install python-telegram-bot # telegram python lib
pip3 install numpy
pip3 install pysocks
sudo apt-get install libatlas-base-dev # numpy error fix
