#!/bin/bash

sudo yum -y update
sudo yum -y upgrade

# set shortcuts ???

yum install libcurl-devel.x86_64

sudo yum install -y git
sudo dnf install -y chromium
sudo yum install -y krusader # total commander analog
sudo yum install -y tmux

# some of pip3 packages require addditional one
pip3 install request --user # past your username instead of user
pip3 install pandas --user
sudo yum install -y python3-devel
pip3 install python-binance --user
pip3 install matplotlib --user
sudo dnf install python3-tkinter

# sublime text
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
sudo dnf install sublime-text
# sublime text configuration

# zsh (after installing zsh, tmux, powerlines reboot the system)
sudo dnf install -y util-linux-user

### install hibernate mode
### install suspend mode 

# install snap package manager
# install vscode
